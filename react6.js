import React from 'react';
import ReactDOM from 'React-DOM';
import ButtonRemove from './ButtonRemove';

export default class ViewSpend extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            arr:[1,2,3,4]
        }
        this.deleteItem = this.deleteItem.bind(this);
    }

    deleteItem(i){
        let temp=[]
        for(var j = 0 ;j<this.state.arr.length;j++){
            
            if(i === j){
            }else{
                temp.push(this.state.arr[j])
            }
        }
        this.setState({
          arr : temp
        })
    }
    
    render(){
        return(
            <div>
                { 
                    this.state.arr.map((arr, i) => ( <div>{arr} <ButtonRemove id={i} deleteItem={this.deleteItem} /></div>))
                }
            </div>
        );
    }
    
}

export default class ButtonRemove extends React.Component {
    constructor(props){
        super(props);
        this.childDelete = this.childDelete.bind(this);
    }
    childDelete(){
        console.log('resetes',this.props);
        this.props.deleteItem(this.props.id);
    }
    render(){
        return(
            <div><button onClick={this.childDelete}>x</button></div>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('root'));