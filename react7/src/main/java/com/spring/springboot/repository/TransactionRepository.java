package com.spring.springboot.repository;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.spring.springboot.model.Transaction;

public interface TransactionRepository extends MongoRepository<Transaction, String> {

}
