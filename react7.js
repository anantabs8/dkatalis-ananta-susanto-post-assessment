import React from 'react';
import ButtonRemove from './ButtonRemove';
import ReactDOM from 'React-DOM';
import Axios from "axios";

export default class ViewSpend extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            entityName:'name',
            price:'1',
            status:'0'
        }
        this.deleteItem = this.deleteItem.bind(this);
    }

    deleteItem(i){
        let temp=[]
        for(var j = 0 ;j<this.state.arr.length;j++){
            
            if(i === j){
            }else{
                temp.push(this.state.arr[j])
            }
        }
        this.setState({
          arr : temp
        })
    }

    async save() {
        let entity = {
            entityName = this.state.entityName,
            price = this.state.price,
            status = this.state.status
        }
        let url="http://localhost:8080/add"
        await Axios.post(url,entity);
    }
    
    render(){
        return(
            <div>
                { 
                    this.state.arr.map((arr, i) => ( <div>{arr} <ButtonRemove id={i} deleteItem={this.deleteItem} /></div>))
                    
               }
               <button onClick={this.save}>save</button>
            </div>
        );
    }
    
}

export default class ButtonRemove extends React.Component {
    constructor(props){
        super(props);
        this.childDelete = this.childDelete.bind(this);
    }
    childDelete(){
        console.log('resetes',this.props);
        this.props.deleteItem(this.props.id);
    }
    render(){
        return(
            <div><button onClick={this.childDelete}>x</button></div>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('root'));