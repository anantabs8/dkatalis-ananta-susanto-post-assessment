import React from 'react';
import ReactDOM from 'React-DOM';
import Input from './Input';

export default class App extends React.Component{
    constructor(props){
        super(props);
    }
    
    render(){
        return(
            <div>
                <Input type="password"/>
            </div>
        );
    }
}

export default class Input extends React.Component {
    render(){
        return(
            <input type = {this.props.type}/>
        );
    }
}

ReactDOM.render(<App />, document.getElementById('root'));