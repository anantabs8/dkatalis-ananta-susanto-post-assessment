package com.spring.springboot.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.springboot.model.Transaction;
import com.spring.springboot.service.TransactionService;

@RestController
@RequestMapping("/add")
public class TransactionController {

	@Autowired
	TransactionService transactionService;

	@PostMapping
	public void addTransaction(@RequestBody Transaction transaction) {
		transactionService.addTransaction(transaction);
	}
	
	@GetMapping
	public List<Transaction> getTransaction() {
		return transactionService.findAll();
	}
}
