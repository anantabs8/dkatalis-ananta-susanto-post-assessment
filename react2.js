import React from 'react';
import ReactDOM from 'React-DOM';

export default class App extends React.Component{
    constructor(props){
        super(props);
        this.state = {
            entity:["id","name","price","status"],
            arr:[1,2,3,4]
        }
    }
    
    render(){
        return(
            <div>
                { this.state.entity.map((entity) => (<div>{entity}</div>))}
                { this.state.arr.map((arr) => ( arr === 0 ? <div>Red</div> : arr === 1 ? 
                <div>Orange</div> : arr === 2 ? <div>Green</div> : <div>{arr}</div>))}
            </div>
        );
    }
}
ReactDOM.render(<App />, document.getElementById('root'));