package com.spring.springboot.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.spring.springboot.model.Transaction;
import com.spring.springboot.service.TransactionService;

@RestController
@RequestMapping("/add")
public class TransactionController {

	@Autowired
	TransactionService transactionService;
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@PostMapping
	public void addTransaction(@RequestBody Transaction transaction) {
		logger.info("start add transaction");
		transactionService.addTransaction(transaction);
	}
	
	@GetMapping
	public List<Transaction> getTransaction() {
		logger.info("start get all transaction");
		return transactionService.findAll();
	}
}
