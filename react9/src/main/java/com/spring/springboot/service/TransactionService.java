package com.spring.springboot.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.spring.springboot.model.Transaction;
import com.spring.springboot.repository.TransactionRepository;

@Service
public class TransactionService {

	@Autowired
	TransactionRepository transactionRepository;

	public void addTransaction(Transaction transaction) {
		transactionRepository.save(transaction);
	}
	
	public List<Transaction> findAll() {
		return transactionRepository.findAllOrderByEntityName();
	}
}
