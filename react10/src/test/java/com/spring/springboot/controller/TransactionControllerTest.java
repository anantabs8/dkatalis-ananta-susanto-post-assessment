package com.spring.springboot.controller;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.spring.springboot.model.Transaction;
import com.spring.springboot.service.TransactionService;

public class TransactionControllerTest {
	private MockMvc mockMvc;

	@InjectMocks
	private TransactionController transactionController;
	
	@Mock
	private TransactionService TransactionService;

	@BeforeEach
	public void init() {
		MockitoAnnotations.initMocks(this);
		mockMvc = MockMvcBuilders.standaloneSetup(transactionController).build();
	}
	
	@Test
    public void findTransactionTest() throws Exception {
        ResultActions resultActions = mockMvc
                .perform(get("/add").contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}
